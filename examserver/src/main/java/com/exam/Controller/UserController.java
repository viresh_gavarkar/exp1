package com.exam.Controller;


import com.exam.helper.UserFoundException;
import com.exam.model.UserRole;
import com.exam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import com.exam.model.*;
import java.util.*;

@RestController
@RequestMapping("/user")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    //Creating User
    @PostMapping("/")
    public User createUser(@RequestBody User user) throws Exception {

     Set<UserRole> roles =new HashSet<>();

     // Encoding password with BcryptpasswordEncoder
        user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
     Role role =new Role();
     role.setRoleId(45L);
     role.setRoleName("Normal");
     UserRole userRole = new UserRole();
     userRole.setUser(user);
     userRole.setRole(role);
     roles.add(userRole);
        return this.userService.createUser(user,roles);
    }

    //
    @GetMapping("/{username}")
    public User getUser(@PathVariable("username") String username)
    {
        return this.userService.getUser(username);
    }

    // Delete User by ID
    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable("userId") Long userId){

        this.userService.deleteUser(userId);
    }

    //Update API

    @ExceptionHandler(UserFoundException.class)
    public ResponseEntity<?> exceptionHandler(UserFoundException ex) {
        return ResponseEntity.ok(ex.getMessage());
    }



}
