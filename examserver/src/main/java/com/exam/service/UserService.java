package com.exam.service;
import com.exam.model.UserRole;
import com.exam.model.User;
import org.springframework.stereotype.Service;

import java.util.*;


public interface UserService {

    //Create USer
    public User createUser(User user, Set<UserRole> userRoles) throws Exception ;

    // Get User by username
    public User getUser(String username);

    // delete User by id

    public void deleteUser(Long userId);



}
