package com.exam.service.impl;
import java.lang.*;

import com.exam.helper.UserFoundException;
import com.exam.model.User;
import com.exam.model.UserRole;
import com.exam.repo.RoleRepository;
import com.exam.repo.UserRepository;
import com.exam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class UserServiceimpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;


    //Creating User
    @Override
    public User createUser(User user, Set<UserRole> userRoles) throws Exception {
      User local=  this.userRepository.findByUsername(user.getUsername());
      if (local!=null){
          System.out.println("User is already present there.....!");

             throw new UserFoundException();



      }else{
          // User Create
          for(UserRole ur: userRoles){
              roleRepository.save(ur.getRole());
          }
         user.getUserRoles().addAll(userRoles);
          local =this.userRepository.save(user);

      }

        return local;
    }


    // Getting User by Username
    @Override
    public User getUser(String username) {
        return this.userRepository.findByUsername(username);
    }


    // Delete user by id
    @Override
    public void deleteUser(Long userId) {

        this.userRepository.deleteById(userId);

    }
}
